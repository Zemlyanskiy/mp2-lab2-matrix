#include "utmatrix.h"

#include <gtest.h>

TEST(TMatrix, can_create_matrix_with_positive_length)
{
	ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
{
	ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
{
	ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
{
	TMatrix<int> m(5);

	ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
{
	TMatrix<int> A(10);
	TMatrix<int> B = A;
	EXPECT_EQ(A, B);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
{
	TMatrix<int> A(2);
	A[0][0] = A[0][1] = A[1][1] = 0;
	TMatrix<int> B = A;
	A[0][1] = 3;
	B[1][1] = 4;

	EXPECT_NE(A, B);
}

TEST(TMatrix, can_get_size)
{
	TMatrix<char> str(10);
	EXPECT_EQ(10, str.GetSize());
}

TEST(TMatrix, can_set_and_get_element)
{
	TMatrix<char> str(4);
	str[1][3] = 'c';
	str[0][0] = 's';
	EXPECT_EQ('c', str[1][3]);
	EXPECT_EQ('s', str[0][0]);
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
{
	TMatrix<int> A(3);
	EXPECT_ANY_THROW(A[-1][2] = 4);
	EXPECT_ANY_THROW(A[1][-2] = 4);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
{
	TMatrix<int> A(4);
	EXPECT_ANY_THROW(A[0][6] = 7);
}

TEST(TMatrix, can_assign_matrix_to_itself)
{
	TMatrix<int> A(10);
	A[1][7] = 8;
	A = A;
	EXPECT_EQ(A[1][7], 8);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
{
	TMatrix<int> A(10), B(10);
	A[1][7] = 8;
	B = A;
	EXPECT_EQ(B[1][7], 8);

}

TEST(TMatrix, assign_operator_change_matrix_size)
{
	TMatrix<int> A(3), B(10);
	B = A;
	EXPECT_EQ(A, B);
}

TEST(TMatrix, can_assign_matrices_of_different_size)
{
	TMatrix<int> A(2), B(4);
	B[1][3] = B[2][2] = 4;
	A = B;
	EXPECT_EQ(A, B);
}

TEST(TMatrix, compare_equal_matrices_return_true)
{
	TMatrix<int> A(7), B(7);
	A[1][1] = B[1][1] = 1;
	EXPECT_EQ(A, B);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
{
	TMatrix<int> A(5);
	A[2][3] = 8;
	EXPECT_TRUE(A == A);
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
{
	TMatrix<int> A(4), B(7);
	EXPECT_FALSE(A == B);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
{
	TMatrix<int> A(7), B(7);
	A[1][1] = B[1][1] = 1;
	A[2][5] = 9;
	B[3][4] = 7;
	B[2][5] = 0;
	A[3][4] = 0;
	A = A + B;

	EXPECT_EQ(A[1][1], 2);
	EXPECT_EQ(A[3][4], 7);
	EXPECT_EQ(A[2][5], 9);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
{
	TMatrix<int> A(6), B(7);
	A[1][1] = B[1][1] = 1;
	A[2][5] = 9;
	B[3][4] = 7;
	EXPECT_ANY_THROW(A = A + B;);

}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
{

	TMatrix<int> A(7), B(7);
	A[1][1] = B[1][1] = 1;
	A[2][5] = 9;
	B[3][4] = 7;
	B[2][5] = 0;
	A[3][4] = 0;
	A = A - B;
	EXPECT_EQ(A[1][1], 0);
	EXPECT_EQ(A[3][4], -7);
	EXPECT_EQ(A[2][5], 9);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
{
	TMatrix<int> A(6), B(7);
	A[1][1] = B[1][1] = 1;
	A[2][5] = 9;
	B[3][4] = 7;
	EXPECT_ANY_THROW(A = A - B;);
}
