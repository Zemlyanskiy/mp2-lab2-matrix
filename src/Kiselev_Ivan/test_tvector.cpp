#include "utmatrix.h"

#include <gtest.h>

TEST(TVector, can_create_vector_with_positive_length)
{
	ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
{
	ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
{
	ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
	ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
{
	TVector<int> v(10);

	ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
{
	TVector<int> A(10);
	TVector<int> B = A;
	EXPECT_EQ(B, A);

}

TEST(TVector, copied_vector_has_its_own_memory)
{
	TVector<int> A(10);
	TVector<int> B = A;
	A[0] = 0;
	B[5] = 7;
	EXPECT_NE(A, B);
}

TEST(TVector, can_get_size)
{
	TVector<int> v(4);

	EXPECT_EQ(4, v.GetSize());
}

TEST(TVector, can_get_start_index)
{
	TVector<int> v(4, 2);

	EXPECT_EQ(2, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
{
	TVector<int> v(4);
	v[0] = 4;

	EXPECT_EQ(4, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
{
	TVector<int> A(10);
	EXPECT_ANY_THROW(A[-1] = 10;);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
{
	TVector<int> A(10);
	EXPECT_ANY_THROW(A[11] = 11;);
}

TEST(TVector, can_assign_vector_to_itself)
{
	TVector<char> str(3);
	str[0] = 'c';
	str[1] = 'a';
	str[2] = 't';
	str = str;
	EXPECT_EQ(str[0], 'c');
	EXPECT_EQ(str[1], 'a');
	EXPECT_EQ(str[2], 't');
}

TEST(TVector, can_assign_vectors_of_equal_size)
{
	TVector<int> A(3), B(3);
	A[1] = 8;
	A[2] = -3;
	B = A;
	EXPECT_EQ(A, B);
}

TEST(TVector, assign_operator_change_vector_size)
{
	TVector<int> A(3), B(9);
	A = B;
	EXPECT_EQ(B.GetSize(), A.GetSize());
}

TEST(TVector, can_assign_vectors_of_different_size)
{
	TVector<int> A(3);
	TVector<int> B(4);
	B[1] = B[3] = 1;
	B[0] = B[2] = 4;
	A = B;
	EXPECT_EQ(A, B);
}

TEST(TVector, compare_equal_vectors_return_true)
{
	TVector<char> str1(3), str2(3);
	str1[0] = str1[2] = 'b';
	str1[1] = 'o';
	str2 = str1;
	EXPECT_TRUE(str1 == str2);
}

TEST(TVector, compare_vector_with_itself_return_true)
{
	TVector<char> str1(3);
	str1[0] = str1[2] = 'b';
	str1[1] = 'o';
	EXPECT_TRUE(str1 == str1);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
{
	TVector<int> A(3), B(1);
	EXPECT_FALSE(A == B);
}

TEST(TVector, can_add_scalar_to_vector)
{
	TVector<int> A(3);
	A[0] = A[1] = A[2] = 0;
	A = A + 3;
	EXPECT_EQ(3, A[0]);
	EXPECT_EQ(3, A[1]);
	EXPECT_EQ(3, A[2]);
}

TEST(TVector, can_subtract_scalar_from_vector)
{
	TVector<int> A(3);
	A[0] = A[1] = A[2] = 0;
	A = A - 3;
	EXPECT_EQ(-3, A[0]);
	EXPECT_EQ(-3, A[1]);
	EXPECT_EQ(-3, A[2]);
}

TEST(TVector, can_multiply_scalar_by_vector)
{
	TVector<int> A(3);
	A[0] = 1;
	A[1] = 0;
	A[2] = 3;
	A = A * 3;
	EXPECT_EQ(3, A[0]);
	EXPECT_EQ(0, A[1]);
	EXPECT_EQ(9, A[2]);
}

TEST(TVector, can_add_vectors_with_equal_size)
{
	TVector<int> A(2), B(2);
	A[0] = B[1] = 2;
	A[1] = B[0] = 3;
	A = A + B;
	EXPECT_EQ(5, A[0]);
	EXPECT_EQ(5, A[1]);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
{
	TVector<int> A(2), B(3);
	A[0] = B[1] = 2;
	A[1] = B[0] = 3;
	B[2] = 3;
	EXPECT_ANY_THROW(A = A + B);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
{
	TVector<int> A(2), B(2);
	A[0] = B[1] = 2;
	A[1] = B[0] = 3;
	A = A - B;
	EXPECT_EQ(-1, A[0]);
	EXPECT_EQ(1, A[1]);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
	TVector<int> A(2), B(3);
	A[0] = B[1] = 2;
	A[1] = B[0] = 3;
	B[2] = 3;
	EXPECT_ANY_THROW(A = A - B);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
{
	int result;
	TVector<int> A(2), B(2);
	A[0] = B[1] = 2;
	A[1] = B[0] = 3;
	result = A * B;
	EXPECT_EQ(12, result);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{

	TVector<int> A(2), B(3);
	A[0] = B[1] = 2;
	A[1] = B[0] = 3;
	B[2] = 3;
	EXPECT_ANY_THROW(A = A * B);
}
