// ����, ���, ���� "������ ����������������-2", �++, ���
//
// sample_matrix.cpp - Copyright (c) ������� �.�. 07.05.2001
//   ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (20.04.2015)
//
// ������������ ����������������� �������

#include <iostream>
#include "utmatrix.h"
//---------------------------------------------------------------------------

void main()
{
  TMatrix<int> a(5), b(5), c(5);
  int i, j;

  setlocale(LC_ALL, "Russian");
  cout << "������������ �������� ��������� ������������� ����������� ������"
    << endl;
  for (i = 0; i < 5; i++)
    for (j = i; j < 5; j++ )
    {
      a[i][j] =  i * 10 + j;
      b[i][j] = (i * 10 + j) * 100;
    }
  c = a + b;
  cout << "Matrix a = " << endl << a << endl;
  cout << "Matrix b = " << endl << b << endl;
  cout << "Matrix c = a + b" << endl << c << endl;
  
  if (a == b)
	  cout << "Matrix a and b is not equal" << endl << endl;
  else
	  cout << "Matrix a and b is equal" << endl << endl;

  c = b - a;

  cout << "Subtract Matrix a and b" << endl << c << endl;

  c = a;

  cout << "Assign Matrix a to c" << endl << c << endl;

  cout << "enter Matrix c " << endl;
  cin >> c;
  cout << c << endl;


}
//---------------------------------------------------------------------------
