# Методы программирования 2: Верхнетреугольные матрицы на шаблонах
## Цели и задачи
В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное хранение матриц специального вида (верхнетреугольных) и выполнение основных операций над ними:
- сложение/вычитание;
- копирование;
- сравнение.
### Реализация верхнетреугольных матриц и выполнение основных операций над ними
#### Описание шаблонного класса TVector:
Верхнетреугольная матрица представлена в виде массива **pVector** с типом **ValType(class)**. То-есть матрица представлена, как вектор векторных элементов.
**Size** - размер вектора.
**StartIndex** - индекс первого элемента вектора

#### Описание шаблонного класса TMatrix:
Шаблонный класс **TMatrix** является дочерним классом от шаблонного класса **TVector**. **TMatrix** public наследует все методы и элементы базового класса **TVector**.

## Реализация utmatrix.h
```c++
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// utmatrix.h - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Верхнетреугольная матрица - реализация на основе шаблона вектора

#ifndef __TMATRIX_H__
#define __TMATRIX_H__

#include <iostream>

using namespace std;

const int MAX_VECTOR_SIZE = 100000000;
const int MAX_MATRIX_SIZE = 10000;

// Шаблон вектора
template <class ValType>
class TVector
{
protected:
  ValType *pVector;
  int Size;       // размер вектора
  int StartIndex; // индекс первого элемента вектора
public:
  TVector(int s = 10, int si = 0);
  TVector(const TVector &v);// конструктор копирования
  ~TVector();
  int GetSize()      { return Size;       } // размер вектора
  int GetStartIndex(){ return StartIndex; } // индекс первого элемента
  ValType& operator[](int pos);             // доступ
  bool operator==(const TVector &v) const;  // сравнение
  bool operator!=(const TVector &v) const;  // сравнение
  TVector& operator=(const TVector &v);     // присваивание

  // скалярные операции
  TVector  operator+(const ValType &val);   // прибавить скаляр
  TVector  operator-(const ValType &val);   // вычесть скаляр
  TVector  operator*(const ValType &val);   // умножить на скаляр
  
  // векторные операции
  TVector  operator+(const TVector &v);     // сложение
  TVector  operator-(const TVector &v);     // вычитание
  ValType  operator*(const TVector &v);     // скалярное произведение
  
  // ввод-вывод
  friend istream& operator>>(istream &in, TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      in >> v.pVector[i];
    return in;
  }
  friend ostream& operator<<(ostream &out, const TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      out << v.pVector[i] << ' ';
    return out;
  }
};

template <class ValType>
TVector<ValType>::TVector(int s, int si)
{
	if (s <= 0 || s > MAX_VECTOR_SIZE)
		throw "Incorrect Size";
	if (si < 0)
		throw "Incorrect Start Index";
	Size = s;
	StartIndex = si;
	pVector = new ValType[s];
} /*------------------------------------------------------------*/

template <class ValType> //конструктор копирования
TVector<ValType>::TVector(const TVector<ValType> &v)
{
	Size = v.Size;
	StartIndex = v.StartIndex;
	pVector = new ValType[v.Size];
	for (int i = 0; i < Size; i++)
		pVector[i] = v.pVector[i];
} /*------------------------------------------------------------*/

template <class ValType>
TVector<ValType>::~TVector()
{
	delete[] pVector;
} /*------------------------------------------------------------*/

template <class ValType> // доступ
ValType& TVector<ValType>::operator[](int pos)
{
	if (pos < StartIndex || pos >= Size + StartIndex)
		throw "Incorrect Index";
	return pVector[pos - StartIndex];
} /*------------------------------------------------------------*/

template <class ValType> // сравнение
bool TVector<ValType>::operator==(const TVector &v) const
{
	if (Size != v.Size || StartIndex != v.StartIndex)
		 return false;
	for (int i = StartIndex; i < Size; i++)
		if (pVector[i] != v.pVector[i])
		{
			return false;
			break;
		}
		return true;
} /*------------------------------------------------------------*/

template <class ValType> // сравнение
bool TVector<ValType>::operator!=(const TVector &v) const
{
	return !(*this == v);
} /*------------------------------------------------------------*/

template <class ValType> // присваивание
TVector<ValType>& TVector<ValType>::operator=(const TVector &v)
{
	if (Size != v.Size)
	{
		delete[] pVector;
		pVector = new ValType[v.Size];
		Size = v.Size;
	}
	StartIndex = v.StartIndex;
	for (int i = 0; i < Size; i++)
		pVector[i] = v.pVector[i];
		return *this;
} /*------------------------------------------------------------*/

template <class ValType> // прибавить скаляр
TVector<ValType> TVector<ValType>::operator+(const ValType &val)
{
	TVector<ValType> temp(Size, StartIndex);
	for (int i = 0; i < Size; i++)
		temp.pVector[i] = pVector[i] + val;
	return temp;
} /*------------------------------------------------------------*/

template <class ValType> // вычесть скаляр
TVector<ValType> TVector<ValType>::operator-(const ValType &val)
{
	TVector<ValType> temp(Size, StartIndex);
	for (int i = 0; i < Size; i++)
		temp.pVector[i] = pVector[i] - val;
	return temp;

} /*------------------------------------------------------------*/

template <class ValType> // умножить на скаляр
TVector<ValType> TVector<ValType>::operator*(const ValType &val)
{
	TVector<ValType> temp(Size, StartIndex);
	for (int i = 0; i < Size; i++)
		temp.pVector[i] = pVector[i] * val;
	return temp;
} /*------------------------------------------------------------*/

template <class ValType> // сложение
TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v)
{
	if (Size != v.Size)
		throw "Different Sizes";
	TVector<ValType> temp(*this);
	for (int i = 0; i < Size; i++)
		 temp.pVector[i] = pVector[i] + v.pVector[i];
	return temp;
} /*------------------------------------------------------------*/

template <class ValType> // вычитание
TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v)
{
	if (Size != v.Size)
		throw "Different Sizes";
	TVector<ValType> temp(*this);
	for (int i = 0; i < Size; i++)
		temp.pVector[i] = pVector[i] - v.pVector[i];
	return temp;
} /*------------------------------------------------------------*/

template <class ValType> // скалярное произведение
ValType TVector<ValType>::operator*(const TVector<ValType> &v)
{
	if (Size != v.Size)
		throw "Different Sizes";
	ValType scal = 0;
	for (int i = 0; i < Size; i++)
		scal += pVector[i] * v.pVector[i];
	return scal;
} /*------------------------------------------------------------*/


// Верхнетреугольная матрица
template <class ValType>
class TMatrix : public TVector<TVector<ValType> >
{
public:
  TMatrix(int s = 10);                           
  TMatrix(const TMatrix &mt);                    // копирование
  TMatrix(const TVector<TVector<ValType> > &mt); // преобразование типа
  bool operator==(const TMatrix &mt) const;      // сравнение
  bool operator!=(const TMatrix &mt) const;      // сравнение
  TMatrix& operator= (const TMatrix &mt);        // присваивание
  TMatrix  operator+ (const TMatrix &mt);        // сложение
  TMatrix  operator- (const TMatrix &mt);        // вычитание

  // ввод / вывод
  friend istream& operator>>(istream &in, TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      in >> mt.pVector[i];
    return in;
  }
  friend ostream & operator<<( ostream &out, const TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      out << mt.pVector[i] << endl;
    return out;
  }
};

template <class ValType>
TMatrix<ValType>::TMatrix(int s): TVector<TVector<ValType> >(s)
{
	if (s <= 0 || s > MAX_MATRIX_SIZE)
		throw "Incorrect Size";
	for (int i = 0; i < s; i++)
		pVector[i] = TVector<ValType>(s - i, i);
} /*------------------------------------------------------------*/

template <class ValType> // конструктор копирования
TMatrix<ValType>::TMatrix(const TMatrix<ValType> &mt):
  TVector<TVector<ValType> >(mt) {}

template <class ValType> // конструктор преобразования типа
TMatrix<ValType>::TMatrix(const TVector<TVector<ValType> > &mt):
  TVector<TVector<ValType> >(mt) {}

template <class ValType> // сравнение
bool TMatrix<ValType>::operator==(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator==(mt);
} /*------------------------------------------------------------*/

template <class ValType> // сравнение
bool TMatrix<ValType>::operator!=(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator!=(mt);
} /*------------------------------------------------------------*/

template <class ValType> // присваивание
TMatrix<ValType>& TMatrix<ValType>::operator=(const TMatrix<ValType> &mt)
{
	TVector<TVector<ValType> >::operator=(mt);
	return *this;
} /*------------------------------------------------------------*/

template <class ValType> // сложение
TMatrix<ValType> TMatrix<ValType>::operator+(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType> >::operator+(mt);
} /*------------------------------------------------------------*/

template <class ValType> // вычитание
TMatrix<ValType> TMatrix<ValType>::operator-(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType> >::operator-(mt);
} /*------------------------------------------------------------*/

// TVector О3 Л2 П4 С6
// TMatrix О2 Л2 П3 С3
#endif
```
## Реализация заготовок тестов, покрывающих все методы класса TVector
```c++
#include "utmatrix.h"
#include <gtest.h>

TEST(TVector, can_create_vector_with_positive_length)
{
  ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
{
  TVector<int> v(10);

  ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
{
	TVector<int> v(3);//v = (6, -5, -9)
	v[0] = 6;
	v[1] = -5;
	v[2] = -9;
	TVector<int> vt(v);

	EXPECT_EQ(v, vt);
}

TEST(TVector, copied_vector_has_its_own_memory)
{
	TVector<int> v(3);//v = (1, 1, -1)
	v[0] = 1;
	v[1] = 1;
	v[2] = -1;

	TVector<int> vt(v);
	TVector<int> *p, *pt;
	p = &v;
	pt = &vt;

	EXPECT_NE(p, pt);
}

TEST(TVector, can_get_size)
{
  TVector<int> v(4);

  EXPECT_EQ(4, v.GetSize());
}

TEST(TVector, can_get_start_index)
{
  TVector<int> v(4, 2);

  EXPECT_EQ(2, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
{
	TVector<int> v(4, 1);
	v[2] = 4;
	
	EXPECT_EQ(4, v[2]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
{
	TVector<int> v(5);

	ASSERT_ANY_THROW(v[-1] = 3);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
{
	TVector<int> v(5);
	
	ASSERT_ANY_THROW(v[7] = 3);
}

TEST(TVector, can_assign_vector_to_itself)
{
	const int size = 3;
	TVector<int> v(size, 1), vt(size, 1);// v = (2, 1, 0)
	v[1] = 2;
	v[2] = 1;
	v[3] = 0;// vt = (2, 1, 0)
	vt[1] = 2;
	vt[2] = 1;
	vt[3] = 0;
	v = v;

	EXPECT_EQ(vt, v);
}

TEST(TVector, can_assign_vectors_of_equal_size)
{
	const int size = 3;
	TVector<int> v(size, 1), vt(size, 1);// v = (2, 1, 0)
	v[1] = 2;
	v[2] = 1;
	v[3] = 0;// vt = (4, -1, 3)
	vt[1] = 4;
	vt[2] = -1;
	vt[3] = 3;
	v = vt;
	
	EXPECT_EQ(vt, v);
}

TEST(TVector, assign_operator_change_vector_size)
{
	const int sizev = 3, sizevt = 1;
	TVector<int> v(sizev, 1), vt(sizevt, 1);// v = (2, 1, 0)
	v[1] = 2;
	v[2] = 1;
	v[3] = 0;
	vt[1] = 10;// vt = (10)
	v = vt;

	EXPECT_NE(sizev, v.GetSize());
}

TEST(TVector, can_assign_vectors_of_different_size)
{
	const int sizev = 3, sizevt = 1;
	TVector<int> v(sizev, 1), vt(sizevt, 1);
	// v = (2, 1, 0)
	v[1] = 2;
	v[2] = 1;
	v[3] = 0;
	vt[1] = 10;// vt = (10)
	v = vt;

	EXPECT_EQ(sizevt, v.GetSize());
}

TEST(TVector, compare_equal_vectors_return_true)
{
	TVector<int> v1(3), v2(3);
	v1[0] = 4; 
	v1[1] = 1; 
	v1[2] = 7; 
	v2[0] = 4;
	v2[1] = 1;
	v2[2] = 7;

	EXPECT_TRUE(v1 == v2);
}

TEST(TVector, compare_vector_with_itself_return_true)
{
	TVector<int> v1(3);
	//v1 = (1, 6, 8)
	v1[0] = 1;
	v1[1] = 6;
	v1[2] = 8;
	
	EXPECT_TRUE(v1 == v1);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
{
	TVector<int> v1(3), v2(4);
	//v1 = (0, 0, 0)
	//v2 = (0, 0, 0, 0)
	v1[0] = 0; 
	v1[1] = 0; 
	v1[2] = 0;
	v2[0] = 0;
	v2[1] = 0;
	v2[2] = 0;
	v2[3] = 0;
	
	EXPECT_FALSE(v1 == v2);
}

TEST(TVector, can_add_scalar_to_vector)
{
	const int size = 5, si = 2;
	const int val = 10;
	TVector<int> v(size, si), vt(size, si);
	//v = (2, 7, 4, 11, 45)
	v[0 + si] = 2;
	v[1 + si] = 7;
	v[2 + si] = 4;
	v[3 + si] = 11;
	v[4 + si] = 45;
	//vt = (12, 17, 14, 21, 55)
	for (int i = si; i < size + si; i++)
		 vt[i] = v[i] + val;

	EXPECT_EQ(vt, v + val);

}

TEST(TVector, can_subtract_scalar_from_vector)
{
	const int size = 5, si = 0;
	const int val = 7;
	TVector<int> v(size, si), vt(size, si);
	//v = (5, 7, 12, 4, 33)
	v[0 + si] = 5;
	v[1 + si] = 7;
	v[2 + si] = 12;
	v[3 + si] = 4;
	v[4 + si] = 33;
	//vt = (-2, 0, 5, -3, 26)
	for (int i = si; i < size + si; i++)
		vt[i] = v[i] - val;

	EXPECT_EQ(vt, v - val);
}

TEST(TVector, can_multiply_scalar_by_vector)
{
	const int size = 5, si = 1;
	const int val = 10;
	TVector<int>v(size, si), vt(size, si);
	//v = (3,4,10,13,2)
	v[0 + si] = 3;
	v[1 + si] = 4;
	v[2 + si] = 10;
	v[3 + si] = 13;
	v[4 + si] = 2;
	//vt = (30,40,100,130,20)
	for (int i = si; i < size + si; i++)
		vt[i] = v[i] * val;

	EXPECT_EQ(vt, v*val);
}

TEST(TVector, can_add_vectors_with_equal_size)
{
	const int size = 4, si = 2;
	TVector<int> v1(size, si), v2(size, si), vt(size, si);
	//v1 = (6, 11, -4, 0)
	v1[0 + si] = 6;
	v1[1 + si] = 11;
	v1[2 + si] = -4;
	v1[3 + si] = 0;
	//v1 = (-12, -3, 8, 15)
	v2[0 + si] = -12;
	v2[1 + si] = -3;
	v2[2 + si] = 8;
	v2[3 + si] = 15;
	//vt = (-6, 8, 4, 15)
	for (int i = 0; i < size; i++)
		vt[i + si] = v1[i + si] + v2[i + si];

	EXPECT_EQ(vt, v1 + v2);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
{
	TVector<int>v1(4), v2(5);

	ASSERT_ANY_THROW(v1 + v2);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
{
	const int size = 4, si = 0;
	TVector<int> v1(size, si), v2(size, si), vt(size, si);
	//v1 = (5, 8, 4, 10)
	v1[0 + si] = 5;
	v1[1 + si] = 8;
	v1[2 + si] = 4;
	v1[3 + si] = 10;
	//v1 = (-4, 3, 5, 13)
	v2[0 + si] = -4;
	v2[1 + si] = 3;
	v2[2 + si] = 5;
	v2[3 + si] = 13;
	//vt = (9, 5, -1, -3)
	for (int i = 0; i < size; i++)
		vt[i + si] = v1[i + si] - v2[i + si];

	EXPECT_EQ(vt, v1 - v2);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
	TVector<int>v1(3), v2(2);

	ASSERT_ANY_THROW(v1 - v2);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
{
	const int size = 4, si = 2;
	int scal = 0;
	TVector<int> v1(size, si), v2(size, si);
	//v1 = (1, 3, 13, 12)
	v1[0 + si] = 1;
	v1[1 + si] = 3;
	v1[2 + si] = 13;
	v1[3 + si] = 12;
	//v1 = (2, 4, 11, 10)
	v2[0 + si] = 2;
	v2[1 + si] = 4;
	v2[2 + si] = 11;
	v2[3 + si] = 10;
	//scal=277
	for (int i = 0; i < size; i++)
		scal += v1[i + si] * v2[i + si];

	EXPECT_EQ(scal, v1 * v2);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{
	TVector<int>v1(8), v2(7);

	ASSERT_ANY_THROW(v1 * v2);
}
```
## Реализация заготовок тестов, покрывающих все методы класса TMatrix
```c++
#include "utmatrix.h"
#include <gtest.h>

TEST(TMatrix, can_create_matrix_with_positive_length)
{
  ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
{
  ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
{
  ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
{
  TMatrix<int> m(5);

  ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
{
	const int size = 2;
	TMatrix<int> m(size);
	for (int i = 0; i < size; i++)
		 for (int j = i; j < size; j++)
				m[i][j] = 1;
	TMatrix<int> mt(m);
	
	EXPECT_EQ(m, mt);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
{
	const int size = 4;
	TMatrix<int> m(size);
	for (int i = 0; i < size; i++)
		 for (int j = i; j < size; j++)
				m[i][j] = 1;
	TMatrix<int>  mt(m);
	TMatrix<int> *p = &m;
	TMatrix<int> *pt = &mt;
	
	EXPECT_NE(p, pt);
}

TEST(TMatrix, can_get_size)
{
	const int size = 5;
	TMatrix<int>m(size);

	EXPECT_EQ(size, m.GetSize());

}

TEST(TMatrix, can_set_and_get_element)
{
	const int size = 5;
	const int val = 4;
	TMatrix<int>m(size);
	m[3][4] = val;

	EXPECT_EQ(val, m[3][4]);
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
{
	const int size = 5;
	const int val = 3;
	TMatrix<int>m(size);

	EXPECT_ANY_THROW(m[-3][4] = val);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
{
	const int size = 3;
	const int val = 4;
	TMatrix<int>m(size);

	EXPECT_ANY_THROW(m[10][1] = val);
}

TEST(TMatrix, can_assign_matrix_to_itself)
{
	const int size = 4;
	TMatrix<int> m(size), mt(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m[i][j] = 2;
			mt[i][j] = 2;
		}

		EXPECT_EQ(mt, m = m);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
{
	const int size = 7;
	TMatrix<int>m(size), mt(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m[i][j] = 4;
	mt = m;

	EXPECT_EQ(mt, m);
}

TEST(TMatrix, assign_operator_change_matrix_size)
{
	const int size = 3, tsize = 5;
	TMatrix<int>m(size), mt(tsize);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m[i][j] = 3;
	m = mt;

	EXPECT_EQ(tsize, m.GetSize());
}

TEST(TMatrix, can_assign_matrices_of_different_size)
{
	const int size = 2, tsize = 4;
	TMatrix<int>m(size), mt(tsize);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m[i][j] = 3;
	m = mt;

	EXPECT_EQ(m, mt);
}

TEST(TMatrix, compare_equal_matrices_return_true)
{
	const int size = 3;
	TMatrix<int>m(size), mt(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m[i][j] = 3;
			mt[i][j] = 3;
		}

	EXPECT_TRUE(m==mt);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
{
	const int size = 3;
	TMatrix<int>m(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m[i][j] = 3;

		EXPECT_TRUE(m == m);
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
{
	const int size = 5,tsize=4;
	TMatrix<int>m(size),mt(tsize);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m[i][j] = 4;
	for (int i = 0; i < tsize; i++)
		for (int j = i; j < tsize; j++)
			mt[i][j] = 4;

	EXPECT_NE(mt,m);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
{
	const int size = 5;
	TMatrix<int>m1(size), m2(size), mt(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m1[i][j] = 4;
			m2[i][j] = 7;
			mt[i][j] = 11;
		}

	EXPECT_EQ(mt, m1 + m2);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
{
	TMatrix<int> m(4), mt(5);

	EXPECT_ANY_THROW(m + mt);
}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
{
	const int size = 5;
	TMatrix<int> m1(size), m2(size), mt(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m1[i][j] = 8;
			m2[i][j] = 5;
			mt[i][j] = 3;
		}
	
	EXPECT_EQ(mt, m1 - m2);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
{
	TMatrix<int> m(8), mt(7);

	EXPECT_ANY_THROW(m - mt);
}
```
### Прохождение всех тестов для шаблонных классов TMatrix и TVector
![image1](https://pp.vk.me/c638126/v638126362/11bce/Yem18GkywcE.jpg)
### Обработка исключений:
**1. Недопустимый размер вектора, матрицы**
**2. Недопустимый индекс элемента вектора, матрицы**
**3. Разные размеры векторов, матриц**
### Модификация примера использования в тестовое приложение, позволяющее задавать матрицы и осуществлять основные операции над ними.
```c++
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// sample_matrix.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (20.04.2015)
//
// Тестирование верхнетреугольной матрицы

#include <iostream>
#include "utmatrix.h"
//---------------------------------------------------------------------------

void main()
{
	try
	{
		TMatrix<int> a(5), b(5), c(5), d(5);
		int i, j, size,num;
		setlocale(LC_ALL, "Russian");
		cout << "Тестирование программ поддержки представления треугольных матриц" << endl;
		cout << "Введите размер матриц A и B:" << endl;
		cin >> size;
		TMatrix<int> A(size), B(size), Temp(size);
		cout << "Введите 1,если хотите ввести матрицу вручную" << endl;
		cin >> num;
		if (num == 1)
		{
			cout << "Введите матрицу А:" << endl;
			cin >> A;
			cout << "Введите матрицу B" << endl;
			cin >> B;
		}
		else
		{
			for (i = 0; i < size; i++)
				 for (j = i; j < size; j++)
				 {
					A[i][j] = rand() % 10;
					B[i][j] = rand() % 10;
				 }
			cout << "Матрица А:" << endl;
			cout << A << endl;
			cout << "Матрица В:" << endl;
			cout << B << endl;
		}
		cout << "A = B ?" << endl;
		if (A == B)
			cout << "Да." << endl;
		else
			cout << "Нет." << endl;
		cout << "A + B = ?" << endl;
		Temp = (A + B);
		cout << Temp << endl;
		cout << "A - B = ?" << endl;
		Temp = A - B;
		cout << Temp << endl;
	}
	catch (const string *err)
	{
		cout << err << endl;
	}
}
//-------------------------------------------------------------------------
```
### Демонстрация использования тестового приложения
**Ввод матрицы вручную**
![image2](https://pp.vk.me/c638126/v638126362/11bea/nfjCLn9LT7k.jpg)
**Автоматическое заполнение матрицы**
![image3](https://pp.vk.me/c638126/v638126362/11be2/SuTj-uFjO7E.jpg)

### Выводы:
Выполнив данную работу мы научились:
- Созданию полноценных реализаций шаблонных классов TMatrix и TVector, проходящих все автоматические тесты
- Реализации заготовок тестов, покрывающих все методы классов TVector и TMatrix
- Модификации примера использования в тестовое приложение, позволяющее задавать матрицы и осуществлять основные операции над ними.





