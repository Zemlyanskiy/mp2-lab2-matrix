#include "utmatrix.h"

#include <gtest.h>

TEST(TMatrix, can_create_matrix_with_positive_length)
//����������� �������� ������� � ������������� ��������
{
  ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
//������������� �������� ����� ������� �������
{
  ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
//������������� �������� ������� � ������������� ������
{
  ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
//�������� ������������ �����������
{
  TMatrix<int> m(5);

  ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
//������� �� ������������ ����������� ����� ��������
{
	const int size = 5;
	TMatrix<int> m1(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	TMatrix<int> m2(m1);

	EXPECT_EQ(m1, m2);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
//������� �� ������������ ����������� ����� ���� �����
{
	const int size = 5;
	TMatrix<int> m1(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	TMatrix<int> m2(m1);

	EXPECT_NE(&m1,&m2);
}

TEST(TMatrix, can_get_size)
//����������� �������� ������ �������
{
	const int size = 5;
	TMatrix<int> m(size);

	EXPECT_EQ(size, m.GetSize());
}

TEST(TMatrix, can_set_and_get_element)
//����������� ���������� � �������� ������� �������
{
	TMatrix<int> m(5);
	m[0][0] = 4;

	EXPECT_EQ(4, m[0][0]);
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
//������������� ���������� �������� �������� � ������������� ��������
{
	TMatrix<int> m(5);

	ASSERT_ANY_THROW(m[-1][0] = 4);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
//������������� ���������� �������� �������� � ����� ������� ��������
{
	TMatrix<int> m(5);

	ASSERT_ANY_THROW(m[MAX_MATRIX_SIZE+1][0] = 4);
}

TEST(TMatrix, can_assign_matrix_to_itself)
//����������� ��������� ������� ����� ����
{
	const int size = 5;
	TMatrix<int> m(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m[i][j] = 1;

	ASSERT_NO_THROW(m = m);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
//����������� ��������� ������� ������ ������� ������ �� �������
{
	const int size = 3;
	TMatrix<int> m1(size),m2(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	m2 = m1;

	EXPECT_EQ(m2 , m1);
}

TEST(TMatrix, assign_operator_change_matrix_size)
//�������� ������������ ������ ������ �������
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	TMatrix<int> m2(2 * size);
	int _size = m2.GetSize();
	m2 = m1;

	EXPECT_NE(_size,m2.GetSize());
}

TEST(TMatrix, can_assign_matrices_of_different_size)
//����������� ��������� ������� ������� ������� �������
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	TMatrix<int> m2(2*size);

	ASSERT_NO_THROW(m2 = m1);
}

TEST(TMatrix, compare_equal_matrices_return_true)
//��������� ������ ������
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	TMatrix<int> m2(m1);

	EXPECT_TRUE(m2 == m1);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
//��������� ������� � ����� �����
{
	const int size = 3;
	TMatrix<int> m(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m[i][j] = 1;

	EXPECT_TRUE(m == m);	
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
//������� ������ ����� �������
{
	const int size = 2;
	TMatrix<int> m1(size), m2(2 * size);

	EXPECT_FALSE(m1 == m2);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
//�������� ������ ������ �����
{
	const int size = 5;
	TMatrix<int> m1(size), m2(size), m3(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
		{
			m1[i][j] = 1;
			m2[i][j] = 2;
			m3[i][j] = 3;
		}

	EXPECT_EQ(m1 + m2, m3);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
//������������� ������� ������� ������ �����
{
	const int size = 2;
	TMatrix<int> m1(size), m2(size * 2);

	ASSERT_ANY_THROW(m1 + m2);
}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
//��������� ������ ������ �����
{
	const int size = 5;
	TMatrix<int> m1(size),m2(size),m3(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
		{
			m1[i][j] = 1;
			m2[i][j] = 2;
			m3[i][j] = -1;
		}

	EXPECT_EQ(m1 - m2, m3);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
//������������� ������� ������� ������ �����
{
	const int size = 2;
	TMatrix<int> m1(size),m2(size*2);

	ASSERT_ANY_THROW(m1 - m2);
}

