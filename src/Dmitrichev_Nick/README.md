# �����
## �� ��������������� ������ �2 �� ��������� "������ ���������������� 2"
# ����: 
## "����������������� ������� �� ��������"


## ���� � ������:

### ����:
� ������ ������������ ������ �������� ������ �������� ����������� �������, �������������� ����������� �������� ������ ������������ ���� (�����������������) � ���������� �������� �������� ��� ����:
��������/���������, �����������, ���������.

### ������� �������� ����������� ������:
1. �������� ��� ���������� ������� ���������
2. ������� ������������� ������
3. ������� ��� ����� �������� ������ �����
4. ������� ��� ������ ��������� ��������� (�������)

������ 4-�� ������ ��������� � ���� ������������ ������

### ������:
- ���������� ������� ���������� ������ `TVector` �������� ��������� ����������.
- ���������� ������� ���������� ������ `TMatrix` �������� ��������� ����������.
- ����������� ����������������� ������ � ������� �������������.
- ���������� ��������� ������, ����������� ��� ������ ������� `TVector` � `TMatrix`.
- ����������� ������� ������������� � �������� ����������, ����������� �������� ������� � ������������ �������� �������� ��� ����.

### ����� ����������� ������ ��� ������� ������-������, ���������� ���������:
- ���������� ������� ������ � ������� (h-����)
- ��������� ����� ������� ������ ��� ������� �� ��������� �������.
- ����� ��������� ������ ��� ������� �� ��������� �������.
- �������� ������ ������������� ������ �������


### ������������ �����������:
- ������� �������� ������ `Git`.
- ��������� ��� ��������� �������������� ������ `Google Test`.
- ����� ���������� `Microsoft Visual Studio` (2008 ��� ������).

### ����� ��������� �������

#### ��������� �������:
- `docs` � ���������� �� ���������� ������������ ������, �������� ���������.
- `gtest` � ���������� `Google Test`.
- `include` � ���������� ��� ���������� ������������ ������.
- `samples` � ���������� ��� ���������� ��������� ����������.
- `sln` � ���������� � ������� ������� � �������� ��� VS 2008 � VS 2010, ��������� ���������� vc9 � vc10 ��������������.
- `src` � ���������� ��� ���������� �������� ����� (cpp-�����).
- `test` � ���������� � ���������� ������� � �������� �����������, ���������������� ������ ������.
- `README.md` � ���������� � �������, ������� �� ������ �������.
- ��������� �����
	- `.gitignore` � �������� ���������� ������, ������������ `Git` ��� ���������� ������ � �����������.
	- `CMakeLists.txt` � �������� ���� ��� ������ ������� � ������� `CMake`. ����� ���� ����������� ��� ��������� ������� � ����� ����������, �������� �� `Microsoft Visual Studio`.
	- `.travis.yml` � ���������������� ���� ��� ������� ��������������� ������������ `Travis-CI`. �����, �������� � ������ ���������� �������, ��������� ����������� �� ��������� ��������������.

### � ������� ���������� ��������� ������:
- ������ `utmatirx`, ���������� ���������� ������� ������ � ������� (���� `./include/utmatrix.h`). 
- ����� ��� ������� ������ � ������� (����� `./test/test_tvector.cpp`, `./test/test_tmatrix.cpp`).
- ������ ������������� ������ ������� (���� `./samples/sample_matrix.cpp`).

### ��������:
- ��������� ��� ������ ���������, ���������� ������� ���������� ��������� ��������������� � ������������ �����. 
- ��� ���� ���������� ������� ������ ���������� �����������.
- � �������� ���������� ������������ ������ ��������� ������������ ������� �������� ������ `Git` � ��������� ��� ���������� �������������� ������ `Google Test`.

### ������ ���������� �� �������������� ��������:
- ����, ���, ���� "������ ����������������-2", �++, ���
- `utmatrix.h` - Copyright (c) ������� �.�. 07.05.2001
- ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (21.04.2015)

#### ������� ������:
- � ������ `TMatrix` ����� (�����) ������������ ��������������� ������ ������ `TVector`
- � ���������� ����� ������� ��� �������������

� ���������� ������� ������������ ������� �����������
##### ���������� ������� `TVector` � `TMatrix` �������� ��������� ����������:
```c++
// ����, ���, ���� "������ ����������������-2", �++, ���
//
// utmatrix.h - Copyright (c) ������� �.�. 07.05.2001
//   ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (21.04.2015)
//
// ����������������� ������� - ���������� �� ������ ������� �������

//1 error. Incorrect Value
//2 error. Error of Indexing
//3 error. Different Sizes

#ifndef __TMATRIX_H__
#define __TMATRIX_H__

#include <iostream>

using namespace std;

const int MAX_VECTOR_SIZE = 100000000;
const int MAX_MATRIX_SIZE = 10000;

// ������ �������
template <class ValType>
class TVector
{
protected:
  ValType *pVector;
  int Size;       // ������ �������
  int StartIndex; // ������ ������� �������� �������
public:
  TVector(int s = 10, int si = 0);
  TVector(const TVector &v);                // ����������� �����������
  ~TVector();
  int GetSize()      { return Size;       } // ������ �������
  int GetStartIndex(){ return StartIndex; } // ������ ������� ��������
  ValType& operator[](int pos);             // ������
  bool operator==(const TVector &v) const;  // ���������
  bool operator!=(const TVector &v) const;  // ���������
  TVector& operator=(const TVector &v);     // ������������

  // ��������� ��������
  TVector  operator+(const ValType &val);   // ��������� ������
  TVector  operator-(const ValType &val);   // ������� ������
  TVector  operator*(const ValType &val);   // �������� �� ������

  // ��������� ��������
  TVector  operator+(const TVector &v);     // ��������
  TVector  operator-(const TVector &v);     // ���������
  ValType  operator*(const TVector &v);     // ��������� ������������

  // ����-�����
  friend istream& operator>>(istream &in, TVector &v)
  {
	  for (int i = 0; i < v.Size; i++)
		  in >> v.pVector[i];
	  return in;
  }
  friend ostream& operator<<(ostream &out, const TVector &v)
  {
	  for (int i = 0; i < v.Size; i++)
		  out << v.pVector[i] << ' ';
	  return out;
  }
};

template <class ValType>
TVector<ValType>::TVector(int s, int si)
{
	if ((s > 0) && (s < MAX_VECTOR_SIZE) && (si >= 0) && (si < MAX_VECTOR_SIZE))
	{
		Size = s;
		StartIndex = si;
		pVector = new ValType[Size];
	}
	else
		throw 1;
} /*��� ������ ���������������� ����������, ���� ��� �� ������������� �������� if*/

template <class ValType> //����������� �����������
TVector<ValType>::TVector(const TVector<ValType> &v):Size(v.Size),StartIndex(v.StartIndex)
{
	pVector = new ValType[Size];
	for (int i = 0; i < Size; ++i)
		pVector[i] = v.pVector[i];
} /*������� �����������������, ��������� ������ � ������������ �����������*/

template <class ValType>
TVector<ValType>::~TVector()
{
	delete[] pVector;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ������
ValType& TVector<ValType>::operator[](int pos)
{
	if ((pos >= StartIndex) && (pos < (Size+StartIndex)))
		return pVector[pos-StartIndex];
	else
		throw 2;
} /*pos ������ ������ � ��������� [StartIndex,Size+StartIndex) � ���������� ������ ������� � ����������� �� ������*/

template <class ValType> // ���������
bool TVector<ValType>::operator==(const TVector &v) const
{
	if (Size == v.Size)
	{
		if (this == &v)//���� ������������ ������ ��� � �����
			return 1;
		else
		{
			int i = 0;
			while ((i < Size) && (pVector[i] == v.pVector[i]))
				i++;
			if (i == Size)
				return 1;
			else
				return 0;
		}
	}
	else
		return 0;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
bool TVector<ValType>::operator!=(const TVector &v) const
{
	return !(*this == v);
} /*������������� operator==*/

template <class ValType> // ������������
TVector<ValType>& TVector<ValType>::operator=(const TVector &v)
{
	if (this != &v)
	{
		if (Size != v.Size)//�� �������� ������ � ������ ���� ������� �������� ���������
		{
			delete[]pVector;
			pVector = new ValType[v.Size];
		}
		Size = v.Size;
		StartIndex = v.StartIndex;
		for (int i = 0;i < Size;++i)
			pVector[i] = v.pVector[i];
	}
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������� ������
TVector<ValType> TVector<ValType>::operator+(const ValType &val)
{
	TVector<ValType> TEMP(*this);
	for (int i = 0;i <Size;++i)
		TEMP.pVector[i]= TEMP.pVector[i]+val;
	return TEMP;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ������� ������
TVector<ValType> TVector<ValType>::operator-(const ValType &val)
{
	TVector<ValType> TEMP(*this);
	for (int i = 0;i <Size;++i)
		TEMP.pVector[i] = TEMP.pVector[i] - val;
	return TEMP;
} /*-------------------------------------------------------------------------*/

template <class ValType> // �������� �� ������
TVector<ValType> TVector<ValType>::operator*(const ValType &val)
{
	TVector<ValType> TEMP(*this);
	for (int i = 0;i < Size;++i)
		TEMP.pVector[i] = TEMP.pVector[i]* val;
	return TEMP;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������
TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v)
{
	if (v.Size == Size)
	{
		TVector<ValType> TEMP(*this);
		for (int i = 0;i < Size;++i)
			TEMP.pVector[i] = TEMP.pVector[i]+ v.pVector[i];
		return TEMP;
	}
	else
		throw 3;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v)
{
	if (v.Size == Size)
	{
		TVector<ValType> TEMP(*this);
		for (int i = 0;i < Size;++i)
			TEMP.pVector[i] = TEMP.pVector[i] - v.pVector[i];
		return TEMP;
	}
	else
		throw 3;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������� ������������
ValType TVector<ValType>::operator*(const TVector<ValType> &v)
{
	
	if (Size == v.Size)
	{
		ValType RES = pVector[0] * v.pVector[0];
		if(Size>1)
			for (int i = 1;i < Size;++i)
				RES = RES+pVector[i] * v.pVector[i];
		return RES;
	}
	else
		throw 3;
} /*-------------------------------------------------------------------------*/


// ����������������� �������
template <class ValType>
class TMatrix : public TVector<TVector<ValType> >
{
public:
  TMatrix(int s = 10);                           
  TMatrix(const TMatrix &mt);                    // �����������
  TMatrix(const TVector<TVector<ValType> > &mt); // �������������� ����
  bool operator==(const TMatrix &mt) const;      // ���������
  bool operator!=(const TMatrix &mt) const;      // ���������
  TMatrix& operator= (const TMatrix &mt);        // ������������
  TMatrix  operator+ (const TMatrix &mt);        // ��������
  TMatrix  operator- (const TMatrix &mt);        // ���������

  // ���� / �����
  friend istream& operator>>(istream &in, TMatrix &mt)
  {
	  for (int i = 0; i < mt.Size; i++)
		  in >> mt.pVector[i];
	  return in;
  }
  friend ostream & operator<<(ostream &out, const TMatrix &mt)
  {
	  for (int i = 0; i < mt.Size; i++)
		  out << mt.pVector[i] << endl;
	  return out;
  }
};

template <class ValType>
TMatrix<ValType>::TMatrix(int s): TVector<TVector<ValType> >(s)
{
	if ((s > 0) && (s < MAX_MATRIX_SIZE))
		for (int i = 0;i < s;++i)
			pVector[i] = TVector<ValType>(s-i, i);
	else
		throw 1;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ����������� �����������
TMatrix<ValType>::TMatrix(const TMatrix<ValType> &mt):
  TVector<TVector<ValType> >(mt) {}

template <class ValType> // ����������� �������������� ����
TMatrix<ValType>::TMatrix(const TVector<TVector<ValType> > &mt):
  TVector<TVector<ValType> >(mt) {}

template <class ValType> // ���������
bool TMatrix<ValType>::operator==(const TMatrix<ValType> &mt) const
{
	return (TVector(*this) == TVector(mt));
} /*�������������� ���� � ������������� operator== ��� �������*/

template <class ValType> // ���������
bool TMatrix<ValType>::operator!=(const TMatrix<ValType> &mt) const
{
	return (TVector(*this) != TVector(mt));
} /*�������������� ���� � ������������� operator!= ��� �������*/

template <class ValType> // ������������
TMatrix<ValType>& TMatrix<ValType>::operator=(const TMatrix<ValType> &mt)
{
	if (this != &mt)
	{
		if (Size != mt.Size)
		{
			delete[]pVector;
			pVector = new TVector<ValType>[mt.Size];
		}
		Size = mt.Size;
		StartIndex = mt.StartIndex;
		for (int i = 0;i < Size;++i)
			pVector[i] = mt.pVector[i];
	}
	return *this;
} /*����� �� TMatrix ��������� � ������� �� TVector, �� ����� �� ��������*/

template <class ValType> // ��������
TMatrix<ValType> TMatrix<ValType>::operator+(const TMatrix<ValType> &mt)
{
	TMatrix<ValType> TEMP(*this);
	return (TEMP = TVector(TEMP) + TVector(mt));
} /*�������������� ���� � ������������� operator+ ��� �������*/

template <class ValType> // ���������
TMatrix<ValType> TMatrix<ValType>::operator-(const TMatrix<ValType> &mt)
{
	TMatrix<ValType> TEMP(*this);
	return (TEMP = TVector(TEMP) - TVector(mt));
} /*�������������� ���� � ������������� operator- ��� �������*/

// TVector �3 �2 �4 �6
// TMatrix �2 �2 �3 �3
#endif
```

#### ����� � �� ����������

��� ��������: �������� ������ ���������� � ����������� �� �������.
��������� ����� ��� ���� ��������, � ��� ������ �������� ����������.

##### ����� ��� ������ `TVector`
```c++
#include "utmatrix.h"

#include <gtest.h>

TEST(TVector, can_create_vector_with_positive_length)
//����������� �������� ������� � ������������� ������
{
  ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
//������������� �������� ����� �������� �������
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
//������������� �������� ������� � ������������� ������
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
//������������� �������� ������� � ������������� ��������� ��������
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
//�������� ������������ �����������
{
  TVector<int> v(3);

  ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
//������ �� ������������ ����������� ����� ���������
{
	const int size = 3;
	TVector<int> v1(size);
	for (int i = 0;i < size;++i)
		v1[i] = 1;
	TVector<int> v2(v1);

	EXPECT_EQ(v1,v2);
}

TEST(TVector, copied_vector_has_its_own_memory)
//������ �� ������������ ����������� ����� ���� �����
{
	const int size = 3;
	TVector<int> v1(size);
	for (int i = 0;i < size;++i)
		v1[i] = 0;
	TVector<int> v2(v1);

	EXPECT_NE(&v1,&v2);
}

TEST(TVector, can_get_size)
//����������� �������� ������ �������
{
	const int size = 3;
	TVector<int> v(size);

	EXPECT_EQ(size, v.GetSize());
}

TEST(TVector, can_get_start_index)
//����������� �������� ��������� ������
{
  const int st_in = 2;
  TVector<int> v(3, st_in);

  EXPECT_EQ(st_in, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
//����������� ���������� � �������� ������� �������
{
  TVector<int> v(4);
  v[0] = 4;

  EXPECT_EQ(4, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
//������������� ���������� �������� �������� � ������������� ��������
{
  TVector<int> v(1);

  ASSERT_ANY_THROW(v[-1]=2);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
//������������� ���������� �������� �������� � ����� ������� ��������
{
	TVector<int> v(1);

	ASSERT_ANY_THROW(v[MAX_VECTOR_SIZE+1] = 2);;
}

TEST(TVector, can_assign_vector_to_itself)
//����������� ��������� ������ ������ ����
{
	const int size = 4;
	TVector<int> v(size);
	for (int i = 0;i < size;++i)
		v[i] = 0;

	ASSERT_NO_THROW(v = v);
}

TEST(TVector, can_assign_vectors_of_equal_size)
//����������� ��������� ������ ������� ������� ������ �� �������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);;
	for (int i = 0;i < size;++i)
		v1[i] = 1;

	EXPECT_EQ(v1, v2=v1);
}

TEST(TVector, assign_operator_change_vector_size)
//�������� ������������ ������ ������ �������
{
	const int size = 4;
	TVector<int> v1(size), v2(2*size);;
	for (int i = 0;i < size;++i)
		v2[i] = 1;
	int _size = v1.GetSize();
	v1 = v2;

	EXPECT_NE(v1.GetSize(),_size);
}

TEST(TVector, can_assign_vectors_of_different_size)
//����������� ��������� ������� ������ ������� �������
{
	const int size = 4;
	TVector<int> v1(size), v2(2 * size);;
	for (int i = 0;i < size;++i)
		v1[i] = 1;

	ASSERT_NO_THROW(v2 = v1);
}

TEST(TVector, compare_equal_vectors_return_true)
//��������� ������ ��������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0;i < size;++i)
		v1[i] = v2[i] = 1;

	EXPECT_TRUE(v1 == v2);
}

TEST(TVector, compare_vector_with_itself_return_true)
//��������� ������� � ����� �����
{
	const int size = 4;
	TVector<int> v(size);
	for (int i = 0;i < size;++i)
		v[i] = 1;

	EXPECT_TRUE(v == v);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
//������� ������ ����� �������
{
	const int size = 4;
	TVector<int> v1(size), v2(2*size);
	for (int i = 0;i < size;++i)
		v1[i] = v2[i] = 1;

	EXPECT_FALSE(v1 == v2);;
}

TEST(TVector, can_add_scalar_to_vector)
//����������� ������� � �������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = v2[i] = 1;
		v2[i] += 2;
	}

	EXPECT_EQ(v1+2,v2);
}

TEST(TVector, can_subtract_scalar_from_vector)
//��������� ������� �� �������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = v2[i] = 1;
		v2[i] -= 2;
	}

	EXPECT_EQ(v1 - 2, v2);
}

TEST(TVector, can_multiply_scalar_by_vector)
//��������� ������� �� ������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = v2[i] = 2;
		v2[i] *= 3;
	}

	EXPECT_EQ(v1 * 3, v2);
}

TEST(TVector, can_add_vectors_with_equal_size)
//�������� �������� ������ �����
{
	const int size = 4;
	TVector<int> v1(size), v2(size),v3(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = v2[i]=1;
		v3[i]=2;
	}

	EXPECT_EQ(v1 +v2, v3);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
//������������� ������� ������� ������ �����
{
	const int size = 4;
	TVector<int> v1(size), v2(2 * size);
	for (int i = 0;i < size;++i)
		v1[i] = v2[i] = 1;
	for (int i = size;i < 2 * size;++i)
		v2[i] = 1;

	ASSERT_ANY_THROW(v1 + v2);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
//��������� �������� ������ �����
{
	const int size = 4;
	TVector<int> v1(size), v2(size), v3(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = v2[i] = 1;
		v3[i] = 0;
	}

	EXPECT_EQ(v1 - v2, v3);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
//������������� ������� ������� ������ �����
{
	const int size = 4;
	TVector<int> v1(size), v2(2 * size);
	for (int i = 0;i < size;++i)
		v1[i] = v2[i] = 1;
	for (int i = size;i < 2 * size;++i)
		v2[i] = 1;

	ASSERT_ANY_THROW(v1 - v2);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
//����������� ����� ��������� ������������ �������� � ������ ������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = 1;
		v2[i] = 0;
	}

	EXPECT_EQ(v1 * v2, 0);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
//������������� ����� ��������� ������������ �������� � ������ ������
{
	const int size = 4;
	TVector<int> v1(size), v2(2*size);
	for (int i = 0;i < size;++i)
		v1[i] = v2[i] = 1;
	for (int i = size;i < 2 * size;++i)
		v2[i] = 1;

	ASSERT_ANY_THROW(v1 * v2);
}


```

##### ������ ������ ��� ������ `TVector`:

![](http://storage7.static.itmages.ru/i/16/1027/h_1477579708_7259225_04315ecb6f.png)
![](http://storage7.static.itmages.ru/i/16/1027/h_1477579708_1503881_04cc7cf442.png)

��� ����� �������� �������!

##### ����� ��� ������ `TMatrix`
```c++
#include "utmatrix.h"

#include <gtest.h>

TEST(TMatrix, can_create_matrix_with_positive_length)
//����������� �������� ������� � ������������� ��������
{
  ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
//������������� �������� ����� ������� �������
{
  ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
//������������� �������� ������� � ������������� ������
{
  ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
//�������� ������������ �����������
{
  TMatrix<int> m(5);

  ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
//������� �� ������������ ����������� ����� ��������
{
	const int size = 5;
	TMatrix<int> m1(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	TMatrix<int> m2(m1);

	EXPECT_EQ(m1, m2);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
//������� �� ������������ ����������� ����� ���� �����
{
	const int size = 5;
	TMatrix<int> m1(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	TMatrix<int> m2(m1);

	EXPECT_NE(&m1,&m2);
}

TEST(TMatrix, can_get_size)
//����������� �������� ������ �������
{
	const int size = 5;
	TMatrix<int> m(size);

	EXPECT_EQ(size, m.GetSize());
}

TEST(TMatrix, can_set_and_get_element)
//����������� ���������� � �������� ������� �������
{
	TMatrix<int> m(5);
	m[0][0] = 4;

	EXPECT_EQ(4, m[0][0]);
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
//������������� ���������� �������� �������� � ������������� ��������
{
	TMatrix<int> m(5);

	ASSERT_ANY_THROW(m[-1][0] = 4);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
//������������� ���������� �������� �������� � ����� ������� ��������
{
	TMatrix<int> m(5);

	ASSERT_ANY_THROW(m[MAX_MATRIX_SIZE+1][0] = 4);
}

TEST(TMatrix, can_assign_matrix_to_itself)
//����������� ��������� ������� ����� ����
{
	const int size = 5;
	TMatrix<int> m(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m[i][j] = 1;

	ASSERT_NO_THROW(m = m);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
//����������� ��������� ������� ������ ������� ������ �� �������
{
	const int size = 3;
	TMatrix<int> m1(size),m2(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	m2 = m1;

	EXPECT_EQ(m2 , m1);
}

TEST(TMatrix, assign_operator_change_matrix_size)
//�������� ������������ ������ ������ �������
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	TMatrix<int> m2(2 * size);
	int _size = m2.GetSize();
	m2 = m1;

	EXPECT_NE(_size,m2.GetSize());
}

TEST(TMatrix, can_assign_matrices_of_different_size)
//����������� ��������� ������� ������� ������� �������
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	TMatrix<int> m2(2*size);

	ASSERT_NO_THROW(m2 = m1);
}

TEST(TMatrix, compare_equal_matrices_return_true)
//��������� ������ ������
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m1[i][j] = 1;
	TMatrix<int> m2(m1);

	EXPECT_TRUE(m2 == m1);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
//��������� ������� � ����� �����
{
	const int size = 3;
	TMatrix<int> m(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
			m[i][j] = 1;

	EXPECT_TRUE(m == m);	
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
//������� ������ ����� �������
{
	const int size = 2;
	TMatrix<int> m1(size), m2(2 * size);

	EXPECT_FALSE(m1 == m2);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
//�������� ������ ������ �����
{
	const int size = 5;
	TMatrix<int> m1(size), m2(size), m3(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
		{
			m1[i][j] = 1;
			m2[i][j] = 2;
			m3[i][j] = 3;
		}

	EXPECT_EQ(m1 + m2, m3);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
//������������� ������� ������� ������ �����
{
	const int size = 2;
	TMatrix<int> m1(size), m2(size * 2);

	ASSERT_ANY_THROW(m1 + m2);
}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
//��������� ������ ������ �����
{
	const int size = 5;
	TMatrix<int> m1(size),m2(size),m3(size);
	for (int i = 0;i < size;++i)
		for (int j = i;j < size;++j)
		{
			m1[i][j] = 1;
			m2[i][j] = 2;
			m3[i][j] = -1;
		}

	EXPECT_EQ(m1 - m2, m3);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
//������������� ������� ������� ������ �����
{
	const int size = 2;
	TMatrix<int> m1(size),m2(size*2);

	ASSERT_ANY_THROW(m1 - m2);
}


```

##### ������ ������ ��� ������ `TMatrix`:

![](http://storage2.static.itmages.ru/i/16/1027/h_1477579905_6335483_9f1259d866.png)

##### ��� ����� �������� �������!

### Sample-����������

�������� ������������ ������������ `utmatrix.h`

![](http://storage6.static.itmages.ru/i/16/1026/h_1477504887_6655620_40a6a2e58f.jpg)

## ������:
- ������� - ������������ ������������ ����� C++, ����������� ������� ��������� ���
- ������� ������������ ������� ������ ������������ �������
- ������� ������ ��������� ����� ������ � ���������� �������